# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration for additional Python modules to be built
# as part of ATLAS externals. A single `pip install` command is used
# for the build/installation. The list of packages is kept in requirements.txt.
#

# The name of the package:
atlas_subdir( PyModules )

# In "release mode" return right away:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Figure out where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3 )
   set( Python_VERSION_MAJOR 3 )
   set( Python_VERSION_MINOR 7 )
else()
   find_package( Python COMPONENTS Interpreter )
   if( "${Python_VERSION}" VERSION_LESS 3 )
      message( WARNING "Python version ${Python_VERSION} not supported. "
         "Package not built." )
      return()
   endif()
endif()

# External(s) needed:
find_package( pip REQUIRED )

# Here we list those dependencies of the packages that will be installed
# with pip and that may or may not be available via LCG. This ensures that the RPM
# dependencies are declared correctly and those packages get installed if needed.
#
# flake8 (see https://github.com/PyCQA/flake8/blob/master/setup.cfg):
find_package( pyflakes )
find_package( pycodestyle )
find_package( mccabe )
find_package( importlib_metadata )  # for flake8 with Python<3.8
find_package( zipp )                # for importlib_metadata

# A common (temporary) installation directory for all python packages:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyModulesBuild" )
set( _pipStamp "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyModules.stamp" )

# List of python modules to install. The exact setup of the requirements file
# depends on whether all the dependencies were found or not. If not, the
# installation is still attempted. (And will likely succeed.) But a warning is
# printed that the build will be non-reproducible...
set( _urlDefinition "--index-url" )
if( ( NOT PYFLAKES_FOUND ) OR ( NOT PYCODESTYLE_FOUND ) OR ( NOT MCCABE_FOUND )
   OR ( NOT IMPORTLIB_METADATA_FOUND ) OR ( NOT ZIPP_FOUND ) )
   message( WARNING "Not all required Python packages were found. They will be"
      " downloaded in a non-reproducible manner!" )
   set( _urlDefinition "--extra-index-url" )
endif()
set( _requirements
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/requirements.txt" )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/requirements.txt.in"
   "${_requirements}" @ONLY )

# Build the package with pip:
add_custom_command( OUTPUT "${_pipStamp}"
   DEPENDS "${_requirements}"
   COMMAND ${CMAKE_COMMAND} -E touch ${_pipStamp}
   COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL PYTHONUSERBASE=${_buildDir}
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   ${PIP_pip_EXECUTABLE} install --disable-pip-version-check --no-warn-script-location --no-warn-conflicts
   --no-cache-dir --user -r "${_requirements}"
   COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../scripts/sanitizePythonScripts.sh "${_buildDir}/bin/*"
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   COMMENT "Building PyModules...")

# Add target and make the package target depend on it:
add_custom_target( pip_install ALL
   DEPENDS "${_pipStamp}" )

add_dependencies( Package_PyModules pip_install )

# Install all built modules at the same time:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Set up the runtime environment for the installed python packages:
configure_file(
   "${CMAKE_CURRENT_SOURCE_DIR}/cmake/PyModulesEnvironmentConfig.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyModulesEnvironmentConfig.cmake"
   @ONLY )
set( PyModulesEnvironment_DIR
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   CACHE PATH "Location of PyModulesEnvironmentConfig.cmake" )
find_package( PyModulesEnvironment REQUIRED )
