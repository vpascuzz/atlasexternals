KLFitter
=======

This package builds the external KLFitter source code, a tool
to reconstruct top quark topologies using likelihood fits.
