# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Locate the GeoModelIO external package.
#
# Defines:
#  GEOMODELIO_FOUND
#  GEOMODELIO_INCLUDE_DIR
#  GEOMODELIO_INCLUDE_DIRS
#  GEOMODELIO_<component>_FOUND
#  GEOMODELIO_<component>_LIBRARY
#  GEOMODELIO_LIBRARIES
#  GEOMODELIO_LIBRARY_DIRS
#
# The user can set GEOMODELIO_ATROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME GeoModelIO
   INCLUDE_SUFFIXES include
   INCLUDE_NAMES GeoModelDBManager/GMDBManager.h
                 GeoModelRead/ReadGeoModel.h
                 GeoModelWrite/WriteGeoModel.h
                 TFPersistification/ACosIO.h
   LIBRARY_SUFFIXES lib
   DEFAULT_COMPONENTS GeoModelDBManager GeoModelRead GeoModelWrite TFPersistification )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GeoModelIO DEFAULT_MSG GEOMODELIO_INCLUDE_DIR
   GEOMODELIO_INCLUDE_DIRS GEOMODELIO_LIBRARIES )
mark_as_advanced( GEOMODELIO_FOUND GEOMODELIO_INCLUDE_DIR GEOMODELIO_INCLUDE_DIRS
   GEOMODELIO_LIBRARIES GEOMODELIO_LIBRARY_DIRS )
