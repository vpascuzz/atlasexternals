# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Locate the GeoModelCore external package.
#
# Defines:
#  GEOMODELCORE_FOUND
#  GEOMODELCORE_INCLUDE_DIR
#  GEOMODELCORE_INCLUDE_DIRS
#  GEOMODELCORE_<component>_FOUND
#  GEOMODELCORE_<component>_LIBRARY
#  GEOMODELCORE_LIBRARIES
#  GEOMODELCORE_LIBRARY_DIRS
#
# The user can set GEOMODELCORE_ATROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME GeoModelCore
   INCLUDE_SUFFIXES include
   INCLUDE_NAMES GeoModelKernel/RCBase.h
                 GeoGenericFunctions/AbsFunction.h
   LIBRARY_SUFFIXES lib
   DEFAULT_COMPONENTS GeoModelKernel GeoGenericFunctions )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GeoModelCore DEFAULT_MSG GEOMODELCORE_INCLUDE_DIR
   GEOMODELCORE_INCLUDE_DIRS GEOMODELCORE_LIBRARIES )
mark_as_advanced( GEOMODELCORE_FOUND GEOMODELCORE_INCLUDE_DIR GEOMODELCORE_INCLUDE_DIRS
   GEOMODELCORE_LIBRARIES GEOMODELCORE_LIBRARY_DIRS )
