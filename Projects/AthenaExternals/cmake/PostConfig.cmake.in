# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# This file is used to configure which externals should be picked up from
# external (AFS/CVMFS) sources.
#

# Find LCG:
set( LCG_VERSION_POSTFIX @LCG_VERSION_POSTFIX@ )
if( AthenaExternals_FIND_QUIETLY )
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED EXACT QUIET )
else()
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED EXACT )
endif()

# Set the platform name for Gaudi, and the externals:
set( ATLAS_PLATFORM @ATLAS_PLATFORM@ )

# Add all custom compilation options:
set( CMAKE_CXX_STANDARD @CMAKE_CXX_STANDARD@
   CACHE STRING "C++ standard used for the build" )
set( CMAKE_CXX_EXTENSIONS @CMAKE_CXX_EXTENSIONS@
   CACHE STRING "(Dis)Allow the usage of C++ extensions" )
add_definitions( -DGAUDI_V20_COMPAT )
add_definitions( -DATLAS_GAUDI_V21 )
add_definitions( -DHAVE_GAUDI_PLUGINSVC )

# Use the -pthread flag for the build instead of the -lpthread linker option,
# whenever possible:
set( THREADS_PREFER_PTHREAD_FLAG TRUE CACHE BOOL
   "Prefer using the -pthread compiler flag over -lpthread" )

# Handle CMP0072 by actively selecting the legacy OpenGL library by default.
set( OpenGL_GL_PREFERENCE "LEGACY" CACHE BOOL
   "Prefer using /usr/lib64/libGL.so unless the user says otherwise" )

# By default do not consider virtual environments in Python's setup.
set( Python_FIND_VIRTUALENV "STANDARD" CACHE STRING
   "Do not use virtualenv or conda to set up Python" )
