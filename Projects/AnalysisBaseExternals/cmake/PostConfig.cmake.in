# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# This file is used to configure the clients of AnalysisBaseExternals.
#

# Find LCG.
set( LCG_VERSION_POSTFIX @LCG_VERSION_POSTFIX@ )
if( AnalysisBaseExternals_FIND_QUIETLY )
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED QUIET )
else()
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED )
endif()

# Set the platform name for Gaudi, and the externals.
set( ATLAS_PLATFORM @ATLAS_PLATFORM@ )

# Add all custom compilation options.
set( CMAKE_CXX_STANDARD @CMAKE_CXX_STANDARD@
   CACHE STRING "C++ standard used for the build" )
set( CMAKE_CXX_EXTENSIONS @CMAKE_CXX_EXTENSIONS@
   CACHE STRING "(Dis)Allow the usage of C++ extensions" )

# Use the -pthread flag for the build instead of the -lpthread linker option,
# whenever possible:
set( THREADS_PREFER_PTHREAD_FLAG TRUE CACHE BOOL
   "Prefer using the -pthread compiler flag over -lpthread" )

# By default do not consider virtual environments in Python's setup.
set( Python_FIND_VIRTUALENV "STANDARD" CACHE STRING
   "Do not use virtualenv or conda to set up Python" )
